import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Loader {

	public static void load(String prefixURL, int part, String subfixURL,
			String saveName, boolean isAudio) throws IOException {
		String fileName = prefixURL + part + subfixURL;

		if (isAudio) {
			fileName += "?audioindex=1";
		}
		// System.out.println(fileName);
		URL link = new URL(fileName);
		// Code to download
		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();

		FileOutputStream fos = new FileOutputStream(saveName, true);
		fos.write(response);
		fos.close();
		// End download code

		System.out.println("Finished " + part);

	}

	public static void main(String[] args) throws IOException {
		
		List<Phim> phims = new ArrayList<Loader.Phim>();
		phims.add(new Phim(
				"DeadPool800",
				"http://n01.hdviet.com/5333a36c604e2cdaab67a984987fb0f3/052016/09/Deadpool_2016_1080p_BluRay_S_ViE/Deadpool_2016_1080p_BluRay_S_ViE_800/chunk_0.ts"));

		for (Phim p : phims) {
			String initURL = p.url;
			String prefix = initURL.substring(0, initURL.lastIndexOf("_") + 1);
			String subfix = initURL.substring(initURL.lastIndexOf(".",
					initURL.length()));

			System.out.println(prefix + "\n" + subfix);
			try {
				int i = 131;
				while (true) {
					load(prefix, i, subfix, "D:/phim/" + p.name + ".ts", true);
					// System.out.println(p.name);
					i++;
				}
			} catch (Exception e) {
				System.out.println("Maybe finish " + p.name);
				e.printStackTrace();
			}
		}
	}

	static class Phim {
		String name;
		String url;

		public Phim(String name, String url) {
			this.name = name;
			this.url = url;
		}

	}

}